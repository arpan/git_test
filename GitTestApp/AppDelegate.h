//
//  AppDelegate.h
//  GitTestApp
//
//  Created by Arpan Desai on 25/02/16.
//  Copyright © 2016 Arpan Desai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

